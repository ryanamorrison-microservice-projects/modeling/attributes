package attributes

import(
  "crypto/rand"  //uses /dev/random or /dev/urandom, ensure haveged (and/or TRNG) is installed
  "errors"
  //"log"
  "math/big"
)

// internal random function used below 
func random(max, min int) (int, error) {
  if min > max {
    return 0, errors.New("Maximum value must be greater than the minimum value.")
  }   
  //max isn't returned as a possibility without this adjustment
  max += 1
  randomNumber,err := rand.Int(rand.Reader, big.NewInt(int64(max-min)))
  if err != nil {
    return 0, err
  }
  randomNumberAdjusted := randomNumber.Add(randomNumber, big.NewInt(int64(min)))
  return int(randomNumberAdjusted.Int64()), nil
}
