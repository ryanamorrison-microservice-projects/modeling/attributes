package attributes

// func PhysicalTotalTraits returns total number of Physical traits assigned
func (ca CharacterAttributesTT) PhysicalTotalTraits() int {
  return ca.physical["Strength"] + ca.physical["Dexterity"] + ca.physical["Stamina"]
}

// func PhysicalTotalTraits returns total number of Social traits assigned
func (ca CharacterAttributesTT) SocialTotalTraits() int {
  return ca.social["Charisma"] + ca.social["Manipulation"] + ca.social["Appearance"]
}

// func PhysicalTotalTraits returns total number of Mental traits assigned
func (ca CharacterAttributesTT) MentalTotalTraits() int {
  return ca.mental["Perception"] + ca.mental["Intelligence"] + ca.mental["Wits"]
}

// func totalTraitsByAssignment is a helper function for the testing functions below
func (ca CharacterAttributesTT) totalTraitsByAssignment(pst string) int {
  switch pst {
    case Physical:
      return ca.physical["Strength"] + ca.physical["Dexterity"] + ca.physical["Stamina"]
    case Social: 
      return ca.social["Charisma"] + ca.social["Manipulation"] + ca.social["Appearance"]
    case Mental:
      return ca.mental["Perception"] + ca.mental["Intelligence"] + ca.mental["Wits"]
    default:
      return 0
  }
}

// func primaryTotalTraits returns total number of traits assigned to 
// the primary category, this is primarily a testing function
func (ca CharacterAttributesTT) primaryTotalTraits() int {
  return ca.totalTraitsByAssignment(ca.primary)
}

// func secondaryTotalTraits returns total number of traits assigned to 
// the secondary category, this is primarily a testing function
func (ca CharacterAttributesTT) secondaryTotalTraits() int {
  return ca.totalTraitsByAssignment(ca.secondary)
}

// func tertiaryTotalTraits returns total number of traits assigned to 
// the tertiary category, this is primarily a testing function
func (ca CharacterAttributesTT) tertiaryTotalTraits() int {
  return ca.totalTraitsByAssignment(ca.tertiary)
}
