package attributes

import(
  "fmt"
)

// func DisplayMatrix displays character attributes as one might find them on a character sheet
func (ca CharacterAttributesTT) DisplayMatrix() {
  fmt.Printf("Physical\t\tSocial\t\t\tMental\n")
  fmt.Printf("Strength: \t%s\tCharisma: \t%s\tPerception: \t%s\n", DisplayAsDots(ca.physical["Strength"]), DisplayAsDots(ca.social["Charisma"]), DisplayAsDots(ca.mental["Perception"]))
  fmt.Printf("Dexterity: \t%s\tManipulation: \t%s\tIntelligence: \t%s\n", DisplayAsDots(ca.physical["Dexterity"]), DisplayAsDots(ca.social["Manipulation"]), DisplayAsDots(ca.mental["Intelligence"]))
  fmt.Printf("Stamina: \t%s\tAppearance: \t%s\tWits: \t\t%s\n", DisplayAsDots(ca.physical["Stamina"]), DisplayAsDots(ca.social["Appearance"]), DisplayAsDots(ca.mental["Wits"]))
}

// func DisplayNumericMatrix displays character attributes in a way that is similar to a character
// sheet however uses numbers instead of dots and returns totals, used for debugging
func (ca CharacterAttributesTT) DisplayNumericMatrix() {
  fmt.Printf("Physical\t\tSocial\t\t\tMental\n")
  fmt.Printf("Strength: %d\t\tCharisma: %d\t\tPerception: %d\n", ca.physical["Strength"], ca.social["Charisma"], ca.mental["Perception"])
  fmt.Printf("Dexterity: %d\t\tManipulation: %d\t\tIntelligence: %d\n", ca.physical["Dexterity"], ca.social["Manipulation"], ca.mental["Intelligence"])
  fmt.Printf("Stamina: %d\t\tAppearance: %d\t\tWits: %d\n", ca.physical["Stamina"], ca.social["Appearance"], ca.mental["Wits"])
  fmt.Printf("Total: %d\t\tTotal: %d\t\tTotal: %d\n", ca.PhysicalTotalTraits(), ca.SocialTotalTraits(), ca.MentalTotalTraits())
}

// func DisplayAsDots converts an interger (attribute number) into dots as 
// they might be displayed on a character sheet
func DisplayAsDots(dotNumber int) string {
  var dotString string
  for i := 1; i <= dotNumber; i++ {
    dotString = dotString + "○"
  }
  //pad leading spaces
  if dotNumber < MaxAttributes {
    var padding int = MaxAttributes - dotNumber
    for i := 1; i <= padding; i++ {
      dotString = " " + dotString
    }
  }
  return dotString
}
