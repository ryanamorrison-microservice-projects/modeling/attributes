package attributes

import(
  "fmt"
  "testing"
)

func TestCharacterAttributesTTStruct(t *testing.T) {
  ca := buildCharacterAttributesTT()
  fmt.Printf("print the struct: %#v\n", *ca)
  fmt.Printf("assigned attributes trait totals: %d %d %d\n", ca.PhysicalTotalTraits(), ca.SocialTotalTraits(), ca.MentalTotalTraits())
}

func TestDisplayMaxtrixTTFunc(t *testing.T) {
  ca := buildCharacterAttributesTT()
  ca.DisplayMatrix()
}

func TestDisplayNumericMatrixTTFunc(t *testing.T) {
  ca := buildCharacterAttributesTT()
  ca.DisplayNumericMatrix()
}

func TestToDots(t *testing.T) {
  for i := 1; i <= 5; i++ {
    fmt.Println(DisplayAsDots(i))
  }
}

func buildCharacterAttributesTT() *CharacterAttributesTT {
  var ca CharacterAttributesTT
  ca.characterID = "123456"
  ca.primary = "physical"
  ca.secondary = "social"
  ca.tertiary = "mental"

  ca.physical = map[string]int{}
  ca.social = map[string]int{}
  ca.mental = map[string]int{}

  ca.physical["Strength"] = 3
  ca.physical["Dexterity"] = 3
  ca.physical["Stamina"] = 4
  ca.social["Charisma"] = 2
  ca.social["Manipulation"] = 3
  ca.social["Appearance"] = 3
  ca.mental["Perception"] = 2
  ca.mental["Intelligence"] = 2
  ca.mental["Wits"] = 2
  return &ca
}
