package attributes

type CharacterAttributes interface {
  DisplayMatrix()
}

const (
  MaxAttributes int = 5
  NumberOfAttributes = 3
)
const (
  Physical string = "Physical"
  Social string = "Social"
  Mental string = "Mental"
  Strength string = "Strength"
  Dexterity string = "Dexterity"
  Stamina string = "Stamina"
  Charisma string = "Charisma"
  Manipulation string = "Manipulation"
  Appearance string = "Appearance"
  Perception string = "Perception"
  Intelligence string = "Intelligence"  
  Wits string = "Wits"
)

//TODO: revise
type AttributeLARP struct {
  label string     //Nimble, Charming, Astute, etc.
  attrType string  //PSM
  dots int         //1-5
  class string     //dexterity-related, manipulation-related, etc.
}
