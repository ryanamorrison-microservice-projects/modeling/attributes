// manage character Attribute traits on a character sheet
package attributes

import(
  "errors"
  "log"
)

var debug_log bool = false 

// the func randomAttributeAssignmentTT() takes a characterID, type and subtypes as strings and
// returns a randomly generated set of Attribute traits for a WOD character sheet in a CharacterAttributesTT struct
func randomAttributeAssignmentTT(characterID, characterType, subType string) (*CharacterAttributesTT, error) {
  ca := CharacterAttributesTT{}
  ca.physical = map[string]int{}
  ca.social = map[string]int{}
  ca.mental = map[string]int{}
  ca.characterID = characterID

  // TODO: collapse these down to defaults once types/subtypes are formalized  
  // change amount of dots getting assigned based character type
  dotAssignmentPool := [3]int{}
  switch characterType {
    case "Ahadi":
      //consulted PGttCB, pp.205-221
      //consulted CB:W20, pp.253-269
      dotAssignmentPool = [3]int{7,5,3}      
    case "Bygone":
      //WOD:BB, p.100
      //SC, p.92
      dotAssignmentPool = [3]int{7,5,3}
    case "Demon":
       switch subType {       
         case "Earthbound":   //usually an antagonist
           //EB,p.65
           dotAssignmentPool = [3]int{8,6,4}     
         default:
           //DtF, p.123
           //DA-DD, p.80
           dotAssignmentPool = [3]int{7,5,3}
       }
    case "Changeling":
      switch subType {
        case "Nunnehi":      //antagonist
          //CtD:1E, p.261
          //CtD:2E, p.276
          dotAssignmentPool = [3]int{9,5,4}
        case "Fomor":        //antagonist
          //CtD:1E, p.262
          //CtD:2E p.276  
          dotAssignmentPool = [3]int{10,6,3}
        default: 
          //CtD:1E, p.123
          //CtD:2E, p.122
          //CtD (20th), p.142
          //DA:F, p.86
          dotAssignmentPool = [3]int{7,5,3}
      }
    case "Fera": 
      //PGttCB, p.161
      //CB:W20, p.207
      dotAssignmentPool = [3]int{7,5,3}
    case "Hengeyokai":
      //H-SotE, p.156  breed does not matter
      //PGttCB, pp.177-203 (consulted)
      //CB:W20, pp.284 (consulted)
      dotAssignmentPool = [3]int{7,5,3}
    case "Hunter":
      switch subType {
        case "Agent":
          //ProjTwil, p.16
          dotAssignmentPool = [3]int{6,4,3}
        case "Ghost Hunter":
          //QatD, p.48
          dotAssignmentPool = [3]int{6,4,3}
        case "Imbued":
          //HtR, p.84
          dotAssignmentPool = [3]int{6,4,3}
        case "SF0":
          //WOD:DHX, p.46
          dotAssignmentPool = [3]int{7,5,3}
        case "Shih":
          //WOD:DHX, p.46
          dotAssignmentPool = [3]int{8,6,3}
        default: 
          //HH:1E, p.58
          //BoM:Rev, p.83 (consulted)
          dotAssignmentPool = [3]int{6,4,3}
      }
    case "Kuei-Jin":
      //KotE, p.73
      dotAssignmentPool = [3]int{7,5,3}
    case "Mage":
      //Consulted Outcasts and Orphans (Hollow Ones) 
      switch subType {
        case "Archmage":
          //MotA, p.81
          dotAssignmentPool = [3]int{8,6,4}
        case "Infernalist":                  //antagonist
          //BoM, p.109 (consulted)
          //BoM:Rev, p.84, p.88 (consulted)
          dotAssignmentPool = [3]int{7,5,3}
        case "Marauder":
          //BoM, p.97 (consulted)
          //BoM:Rev, pp.62-63 (consulted)
          //primary should be P or M
          dotAssignmentPool = [3]int{7,5,3}
        case "Nephandus":
          //BoM, p.37 (consulted)
          //BoM:Rev, chapter 1 (consulted)
          //primary should be P or S
          dotAssignmentPool = [3]int{7,5,3}
        case "Oracle":                      //NPC
          //MotA, p.81
          dotAssignmentPool = [3]int{8,6,4}
        default:
          //MtA:Rev, p.87
          //MtA:2E, p.104 (WW4300)
          //SC, p.91
          dotAssignmentPool = [3]int{7,5,3}
       }
    case "Mortal":
      switch subType {
        case "Arcanum":
          //HotA, p.52
          dotAssignmentPool = [3]int{6,4,3}
        case "Consor":
          //ARH, p.71
          //SC, p.92
          dotAssignmentPool = [3]int{6,4,3}
        case "Ghoul":
          //G-FA, p.69
          //LL&L, p.60
          //GaR, p.116
          dotAssignmentPool = [3]int{6,4,3}
        case "Gypsy":
          //WOD-Gyp, p.42
          dotAssignmentPool = [3]int{6,4,3}
        case "Kinfolk":
          //K-UH, p.48
          //K-aBA:20, p.57 & WtA:20, p.379
          dotAssignmentPool = [3]int{6,4,3}
        case "Medium":
          //M-SwtD, p.16
          dotAssignmentPool = [3]int{6,4,3}
        case "Sorcerer":
          //WOD-Sorc, p.52
          //Sorc:Rev, p.45
          dotAssignmentPool = [3]int{6,4,3}
        case "Thrall":
          //DaD, p.60
          //DA-DD, p.80
          //IPoS, p.69 (N/A)
          //BoM, p.109 (consulted)
          //BoM:Rev, p.84, p.88 (consulted)
          dotAssignmentPool = [3]int{6,4,3}
        //default:
        //  dotAssignmentPool := [3]int{6,4,3}
      }
    case "Mummy":
      switch subType {
        case "Amenti":
          //MtR, p.56
          dotAssignmentPool = [3]int{6,4,3}
        default:
          //WOD Mummy, p.12
          //WOD Mummy 2E, p.48
          dotAssignmentPool = [3]int{7,5,3}
      }
    case "Possessed":
      switch subType {
        case "Drone":		
          //Pos, p.98
          dotAssignmentPool = [3]int{6,4,3}         
        case "Fomor":		//this can be a point of confusion with the Changeling antagonist
          //FL-aPGttF,p.18
          //Pos, p.98
          dotAssignmentPool = [3]int{6,4,3}
        case "Gorgon":		
          //Pos, p.98
          dotAssignmentPool = [3]int{6,4,3}
        case "Kami":		
          //Pos, p.98
          dotAssignmentPool = [3]int{6,4,3}
      }
    case "Spectre":
      switch subType {
        case "Doppleganger":
          //DR-S, p.29-31
          dotAssignmentPool = [3]int{7,5,3}
        case "Malfean":
          //DR-S, p.29-31
          dotAssignmentPool = [3]int{7,5,3}
        case "Mortwright":
          //DR-S, p.29-31
          dotAssignmentPool = [3]int{7,5,3}
        case "Nephwrack":
          //DR-S, p.29-31
          dotAssignmentPool = [3]int{7,5,3}
        case "Shade":
          //DR-S, p.29-31
          dotAssignmentPool = [3]int{7,5,3}
        case "Stripling":
          //DR-S, p.29-31
          dotAssignmentPool = [3]int{6,4,2}
      }
    case "Vampire": 
      //consulted Outcasts (Caitiff)
      switch subType {
        case "Elder":
          //E-tEW, p.61
          dotAssignmentPool = [3]int{10,7,5}
        default:
          //V-tDA, p.104
          //V-tDA (20th), p.152
          //VtM:3e, p.103
          dotAssignmentPool = [3]int{7,5,3}
      } 
    //case spirit 
    //there are other (antagonist) spirits on p.263 of CtD:1E
    //also see the BoM's for infernal spirit hierarchies
    case "Werewolf":
      //consulted Outcasts (Ronin)
      //DA-W,p.80
      //WtA:Rev,p.100
      //WtA:20, p.112
      dotAssignmentPool = [3]int{7,5,3}
    case "Wraith": 
      switch subType {
        case "Jade":      //their attributes are the same as Stygia but they have other traits that differ
          //DKoJ, p.124
          dotAssignmentPool = [3]int{7,5,3}
        default:
          //WtO 2E, p.98
          dotAssignmentPool = [3]int{7,5,3}
      }
    default:
      return nil,errors.New("invalid character type") 
  }  
  if debug_log == true { log.Printf("DEBUG: %v\n", dotAssignmentPool) }

  // class decision loop vars
  classPool := [3]string{Physical, Social, Mental}
  secondaryClassPool := [2]string{}
  var thisClass string

  for _,dotPool := range dotAssignmentPool {
    if ca.primary == "" {
      chosenClass,err := random(2, 0)
      if err != nil {
        log.Fatal(err)
      }
      ca.primary = classPool[chosenClass]
      thisClass = ca.primary
      switch ca.primary {
        case Physical:
          secondaryClassPool = [2]string{Social, Mental}
        case Social:
          secondaryClassPool = [2]string{Physical, Mental}
        case Mental:
          secondaryClassPool = [2]string{Physical, Social}
      }
      if debug_log == true { log.Printf("DEBUG: %s selected as Primary\n", thisClass) }
    } else if ca.secondary == "" {
      chosenClass,err := random(1, 0)
      if err != nil {
        log.Fatal(err)
      }
      ca.secondary = secondaryClassPool[chosenClass]
      thisClass = ca.secondary
      if debug_log == true { log.Printf("DEBUG: %s selected as Secondary\n", thisClass) }
    } else {
      if (ca.primary == Physical && ca.secondary == Social) || (ca.primary == Social && ca.secondary == Physical) {
        ca.tertiary = Mental
        thisClass = ca.tertiary
      } else if (ca.primary == Physical && ca.secondary == Mental) || (ca.primary == Mental && ca.secondary == Physical) {
        ca.tertiary = Social
        thisClass = ca.tertiary
      } else if (ca.primary == Social && ca.secondary == Mental) || (ca.primary == Mental && ca.secondary == Social) {
        ca.tertiary = Physical
        thisClass = ca.tertiary
      } else {
        return nil,errors.New("logical error assigning PSM traits")
      }
      if debug_log == true { log.Printf("DEBUG: %s selected as Tertiary\n", thisClass) }
    }

    chosenDots := [3]int{1,1,1}
    if debug_log == true { log.Printf("DEBUG: dotPool for %s = %d\n", thisClass, dotPool) }

    min := 1
    max := 0
    j := 0
    //1 freebie per trait
    adjustedMaxAttributes := MaxAttributes - 1
    for {
      if dotPool == 0 {
        //nothing else to assign
        break 
      } else {
        max = dotPool
        if max > adjustedMaxAttributes {
          max = adjustedMaxAttributes
        }
        if debug_log == true { log.Printf("DEBUG: max = %d\n", max) }
        //select a random number of dots to give to the attribute
        chosenNumber,err := random(max, min)
        if err != nil {
          log.Fatal(err)
        }
        if debug_log == true { log.Printf("DEBUG: chosen number = %d\n", chosenNumber) }
        if (chosenDots[j] + chosenNumber) > MaxAttributes {
          if debug_log == true { log.Printf("DEBUG: chosenDots[%d] is %d + chosenNumber is %d\n", j, chosenDots[j], chosenNumber) } 
          chosenNumber = MaxAttributes - chosenDots[j]
          if debug_log == true { log.Printf("DEBUG: chosenNumber adjusted to %d (MaxAttributes - chosenDots)", chosenNumber) }
        } 
        chosenDots[j] = chosenDots[j] + chosenNumber
        if debug_log == true { log.Printf("DEBUG: chosenDots[%d] = %d\n", j, chosenNumber) }      
        dotPool = dotPool - chosenNumber
        if debug_log == true { log.Printf("DEBUG: updated dotPool = %d\n", dotPool) }     
        j++
        if j == 3 && dotPool > 0 {
            //start from beginning of array again, more points remaining
            j = 0
        }
      }
    }

    // Handle special case where random chose 
    // 1,1,X where X is all/most of the points
    // in a single trait (AKA "Min-Maxing")
    counter := 0
    largestTrait := 0
    largestIdx := 0
    recipIdx := 0
    for i,currentTrait := range chosenDots {
      if currentTrait == 1 {
         counter++
         //this will always be the last encountered
         recipIdx = i  
      }
      if currentTrait > largestTrait {
        largestTrait = currentTrait
        largestIdx = i
      } 
    }  
    if counter == 2 {
      if debug_log == true { log.Printf("DEBUG: largest value before - chosenDots[%d] = %d\n", largestIdx, chosenDots[largestIdx]) }
      chosenDots[largestIdx] = chosenDots[largestIdx] - 1
      if debug_log == true { log.Printf("DEBUG: largest adj. value - chosenDots[%d] = %d\n", largestIdx, chosenDots[largestIdx]) }
      if debug_log == true { log.Printf("DEBUG: recp. value before - chosenDots[%d] = %d\n", recipIdx, chosenDots[recipIdx]) }
      chosenDots[recipIdx] = chosenDots[recipIdx] + 1
      if debug_log == true { log.Printf("DEBUG: recp. adj. value - chosenDots[%d] = %d\n", recipIdx, chosenDots[recipIdx]) }
    }
      
    switch thisClass {
      case Physical:
        ca.physical[Strength] = chosenDots[0]
        ca.physical[Dexterity] = chosenDots[1]
        ca.physical[Stamina] = chosenDots[2]
      case Social:
        ca.social[Charisma] = chosenDots[0]
        ca.social[Manipulation] = chosenDots[1]
        ca.social[Appearance] = chosenDots[2]
      case Mental:
        ca.mental[Perception] = chosenDots[0]
        ca.mental[Intelligence] = chosenDots[1]
        ca.mental[Wits] = chosenDots[2]
    }
  }
  return &ca, nil
}
