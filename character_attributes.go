package attributes

type CharacterAttributesTT struct {
  // ID of character the attributes are assigned to
  characterID   string
  // attribute order at creation time (helpful for testing)
  primary       string
  secondary     string
  tertiary      string
  // the nine attributes in three classes
  physical      map[string]int
  social        map[string]int
  mental        map[string]int
}
