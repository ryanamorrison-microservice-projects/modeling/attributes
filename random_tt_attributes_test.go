package attributes

import(
  "fmt"
  "testing"
)

type testData struct {
  id       string
  charType string
  subType  string
  wantP    int
  wantS    int
  wantT    int
}

func loadTestData() []testData {
  tests := []testData{}
  tests = append(tests, testData{id: "123456", charType: "Mage", subType: "Tradition", wantP: 7, wantS: 5, wantT: 3})
  tests = append(tests, testData{id: "123457", charType: "Demon", subType: "Earthbound", wantP: 8, wantS: 6, wantT: 4})
  tests = append(tests, testData{id: "123458", charType: "Changeling", subType: "Nunnehi", wantP: 9, wantS: 5, wantT: 4})
  tests = append(tests, testData{id: "123459", charType: "Changeling", subType: "Fomor", wantP: 10, wantS: 6, wantT: 3})
  tests = append(tests, testData{id: "123459", charType: "Hunter", subType: "Shih", wantP: 8, wantS: 6, wantT: 3})
  tests = append(tests, testData{id: "123459", charType: "Mortal", subType: "Arcanum", wantP: 6, wantS: 4, wantT: 3})
  tests = append(tests, testData{id: "123459", charType: "Spectre", subType: "Stripling", wantP: 6, wantS: 4, wantT: 2})
  tests = append(tests, testData{id: "123459", charType: "Vampire", subType: "Elder", wantP: 10, wantS: 7, wantT: 5})
  return tests
}

func TestRandomTTCharacterAttributeGeneration(t *testing.T) {
  tests := loadTestData()

  for i,_ := range tests {
    ca,err := randomAttributeAssignmentTT(tests[i].id, tests[i].charType, tests[i].subType)
    if err != nil {
      t.Error(err)
    }
    //the +3 are the +1 freebies for each of the traitsi (e.g. Strength, Dexterity, Stamina) 
    //in each of the three categories (Physical, Social, Mental)
    want := [3]int{(tests[i].wantP +3),(tests[i].wantS +3),(tests[i].wantT +3)}
    got := [3]int{ca.primaryTotalTraits(), ca.secondaryTotalTraits(), ca.tertiaryTotalTraits()}
    if got != want {
      t.Errorf("Testing Error, want %#v but got %#v\n", want, got)
    } else {
      fmt.Printf("%s %s test:\n", tests[i].subType, tests[i].charType)
      ca.DisplayNumericMatrix()
    }
  }
}



