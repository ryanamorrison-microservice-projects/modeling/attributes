package attributes

import(
  "fmt"
  "testing"
)

func TestRandom(t *testing.T) {
  for i := 0; i < 100; i++ {
    zeroTest,err := random(9,0)
    if err != nil {
      t.Error(err)
    }
    fmt.Printf("%d  ", zeroTest)
  }
  fmt.Printf("\n")
  for i := 0; i < 256; i++ {
    firstPSM,err := random(3,1)
    if err != nil {
      t.Error(err)
    }
    fmt.Printf("%d  ", firstPSM)
  }
  fmt.Printf("\n")
  for i := 0; i < 256; i++ {
    secondPSM,err := random(2,1)
    if err != nil {
      t.Error(err)
    }
    fmt.Printf("%d  ", secondPSM)
  }
  fmt.Printf("\n")

}
